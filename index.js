const functions = require("firebase-functions");

const express = require("express");
const app = express();

const mongoose = require("mongoose");

const bodyParser = require("body-parser");
const homeRoute = require("./routes/home");
const postsRoute = require("./routes/posts");
require("dotenv/config");
const cors = require("cors");
app.use(cors({ origin: true }));

app.use(bodyParser.json());

// Routes
app.use("/", homeRoute);
app.use("/posts", postsRoute);

const uri = process.env.DB_CONNECTION;

// Connect to mongodb
mongoose.connect(uri,
    () => {
        console.log("connected to db");
    });

app.listen(3000);

exports.app = functions.https.onRequest(app);
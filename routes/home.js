const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
    res.status(200).send("NODE EXPRESS REST API<br><a href='/app/posts'>Alle Posts</a>");
});

module.exports = router;
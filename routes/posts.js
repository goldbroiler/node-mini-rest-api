const express = require("express");
const router = express.Router();
const Post = require("../models/Post");

router.get("/", (req, res) => {
    Post.find({}).then(data => res.status(200).send(data));
});

router.post("/", (req, res) => {
    const postObject = {
        ...req.body,
    };

    const post = new Post(postObject);

    post.save().then(data => res.json(data)).catch(err => res.json(err));
});


module.exports = router;
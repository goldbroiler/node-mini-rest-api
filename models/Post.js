// mongodb data prepared as a model
const mongoose = require("mongoose");

const PostSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    period: {
        type: String,
        required: true,
    },
    url: {
        type: String,
    },
    content: {
        type: String,
    },
    work: {
        bsonType: Object,
    },
    skills: {
        type: String,
    },
});


module.exports = mongoose.model("Post", PostSchema);